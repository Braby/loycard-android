package at.fhooe.s1510455003.loycard.activities;

import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.zxing.WriterException;

import at.fhooe.s1510455003.loycard.R;
import at.fhooe.s1510455003.loycard.data.Card;
import at.fhooe.s1510455003.loycard.util.BarcodeUtil;
import at.fhooe.s1510455003.loycard.util.ResultCodes;

public class BarcodeDetailActivity extends AppCompatActivity {
    public static final String EXTRA_CARD = "card";

    private Card mCard;
    private ImageView mImageView;
    private TextView mTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_barcode_detail);

        mCard = getIntent().getParcelableExtra(EXTRA_CARD);
        if (mCard == null) finishWithError();

        mImageView = (ImageView) findViewById(R.id.barcode_detail_image_view);
        mTextView = (TextView) findViewById(R.id.barcode_detail_text_view);

        mTextView.setText(mCard.getBarcode());
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        if (hasFocus) {
            try {
                Bitmap barcodeImage = BarcodeUtil.encodeAsBitmap(mCard.getBarcode(), mCard.getBarcodeFormat(), mImageView.getWidth(), mImageView.getHeight());
                mImageView.setImageBitmap(barcodeImage);
            } catch (WriterException e) {
                e.printStackTrace();
            }
        }

        super.onWindowFocusChanged(hasFocus);
    }

    private void finishWithError() {
        setResult(ResultCodes.RESULT_CODE_ERROR);
        finish();
    }
}
