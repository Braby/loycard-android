package at.fhooe.s1510455003.loycard.activities;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;

import at.fhooe.s1510455003.loycard.R;
import at.fhooe.s1510455003.loycard.data.Card;
import at.fhooe.s1510455003.loycard.db.CardContract;
import at.fhooe.s1510455003.loycard.db.DbHelper;
import at.fhooe.s1510455003.loycard.util.BarcodeUtil;
import at.fhooe.s1510455003.loycard.util.ResultCodes;
import at.fhooe.s1510455003.loycard.util.SnackUtil;

public class CardDetailsActivity extends AppCompatActivity {
    public static final int REQUEST_CODE_EDIT_CARD = 2;
    public static final int REQUEST_CODE_BARCODE_DETAIL = 3;
    public static final String CARD_ID = "cardId";

    private long mCardId;
    private Card mCard;
    private Bitmap mBarcodeImage;

    private LinearLayout mCardDetailsLayout;
    private TextView mTvCardName;
    private TextView mTvBarcode;
    private TextView mTvNotes;
    private ImageView mIvBarcode;
    private ImageView mIvCardImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_details);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(CardDetailsActivity.this, CreateEditCardActivity.class);
                intent.putExtra(CreateEditCardActivity.CARD, mCard);
                startActivityForResult(intent, REQUEST_CODE_EDIT_CARD);
            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mCardDetailsLayout = (LinearLayout) findViewById(R.id.card_details_layout);
        mTvCardName = (TextView) findViewById(R.id.text_view_card_name);
        mTvBarcode = (TextView) findViewById(R.id.text_view_barcode);
        mTvNotes = (TextView) findViewById(R.id.text_view_notes);
        mIvBarcode = (ImageView) findViewById(R.id.image_view_barcode);
        mIvCardImage = (ImageView) findViewById(R.id.image_view_card_image);

        mCardId = getIntent().getLongExtra(CARD_ID, -1);
        if (mCardId == -1) finishWithError();

        getCardDetails();
        if (mCard == null) finishWithError();

        mIvBarcode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(CardDetailsActivity.this, BarcodeDetailActivity.class);
                i.putExtra(BarcodeDetailActivity.EXTRA_CARD, mCard);
                startActivityForResult(i, REQUEST_CODE_BARCODE_DETAIL);
            }
        });
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        if (hasFocus) {
            updateViews();
        }

        super.onWindowFocusChanged(hasFocus);
    }

    private void getCardDetails() {
        SQLiteDatabase db = DbHelper.getInstance(this).getReadableDatabase();
        String selection = CardContract.CardEntry._ID + " LIKE ?";
        String[] selectionArgs = { String.valueOf(mCardId) };
        Cursor c = db.query(CardContract.CardEntry.TABLE_NAME,
                null, selection, selectionArgs, null, null, null);
        if (c.moveToFirst()) {
            mCard = new Card();
            mCard.setId(c.getLong(c.getColumnIndex(CardContract.CardEntry._ID)));
            mCard.setName(c.getString(c.getColumnIndex(CardContract.CardEntry.COLUMN_NAME_NAME)));
            mCard.setBarcode(c.getString(c.getColumnIndex(CardContract.CardEntry.COLUMN_NAME_BARCODE)));
            String barcodeFormatString = c.getString(c.getColumnIndex(CardContract.CardEntry.COLUMN_NAME_BARCODE_FORMAT));
            if (barcodeFormatString != null) mCard.setBarcodeFormat(BarcodeFormat.valueOf(barcodeFormatString));
            mCard.setNotes(c.getString(c.getColumnIndex(CardContract.CardEntry.COLUMN_NAME_NOTES)));
            String uriString = c.getString(c.getColumnIndex(CardContract.CardEntry.COLUMN_NAME_IMAGE_URI));
            if (uriString != null) mCard.setImageUri(Uri.parse(uriString));
        }
        c.close();
    }

    private void updateViews() {
        mTvCardName.setText(mCard.getName());
        mTvBarcode.setText(mCard.getBarcode());
        try {
            mBarcodeImage = BarcodeUtil.encodeAsBitmap(mCard.getBarcode(), mCard.getBarcodeFormat(), mCardDetailsLayout.getWidth(), mIvBarcode.getHeight());
            mIvBarcode.setImageBitmap(mBarcodeImage);
        } catch (WriterException e) {
            e.printStackTrace();
        }
        mTvNotes.setText(mCard.getNotes());
        if (mCard.getImageUri() != null) {
            new AsyncTask<Uri, Void, Bitmap>() {
                @Override
                protected Bitmap doInBackground(Uri... params) {
                    if (params.length > 0 && params[0] != null) {
                        return BitmapFactory.decodeFile(params[0].getPath());
                    }
                    return null;
                }
                @Override
                protected void onPostExecute(Bitmap bitmap) {
                    if (bitmap != null) mIvCardImage.setImageBitmap(bitmap);
                }
            }.execute(mCard.getImageUri());
        }
    }

    @Override
     protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_EDIT_CARD && resultCode == RESULT_OK) {
            getCardDetails();
            updateViews();
            setResult(RESULT_OK);
        }
        if (resultCode == ResultCodes.RESULT_CODE_ERROR) {
            SnackUtil.showSnackBar(mCardDetailsLayout, R.string.error_general);
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    private void finishWithError() {
        setResult(ResultCodes.RESULT_CODE_ERROR);
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_card_details, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_delete_card:
                deleteCard();
                return true;
            case R.id.action_show_map:
                showMap();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void deleteCard() {
        SQLiteDatabase db = DbHelper.getInstance(this).getWritableDatabase();
        String selection = CardContract.CardEntry._ID + " LIKE ?";
        String[] selectionArgs = { String.valueOf(mCardId) };
        int count = db.delete(CardContract.CardEntry.TABLE_NAME, selection, selectionArgs);
        if (count > 0) {
            Intent i = new Intent();
            i.putExtra(CARD_ID, mCardId);
            setResult(ResultCodes.RESULT_CODE_DELETED, i);
            finish();
        }
    }

    private void showMap() {
        Intent i = new Intent(this, MapsActivity.class);
        i.putExtra(MapsActivity.EXTRA_CARD_NAME, mCard.getName());
        startActivity(i);
    }
}
