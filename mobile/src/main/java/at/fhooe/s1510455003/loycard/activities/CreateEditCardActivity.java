package at.fhooe.s1510455003.loycard.activities;

import android.Manifest;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import at.fhooe.s1510455003.loycard.R;
import at.fhooe.s1510455003.loycard.data.Card;
import at.fhooe.s1510455003.loycard.db.CardContract;
import at.fhooe.s1510455003.loycard.db.DbHelper;
import at.fhooe.s1510455003.loycard.util.FileUtil;
import at.fhooe.s1510455003.loycard.util.SnackUtil;

public class CreateEditCardActivity extends AppCompatActivity {
    public static final int REQUEST_CODE_CAPUTRE_IMAGE = 100;
    private static final int REQUEST_CODE_PERMISSION_CAMERA = 200;
    public static final String CARD = "card";

    private boolean mIsEditing;
    private Card mCard;
    private Uri mImageUri;

    private LinearLayout mLayout;
    private Button mButtonChooseImage;
    private ImageView mImageViewCardImage;
    private TextInputLayout mTextInputLayoutCardName;
    private EditText mEditTextCardName;
    private TextInputLayout mTextInputLayoutBarcode;
    private EditText mEditTextBarcode;
    private TextInputLayout mTextInputLayoutBarcodeFormat;
    private EditText mEditTextBarcodeFormat;
    private Button mButtonScanBarcode;
    private TextInputLayout mTextInputLayoutNotes;
    private EditText mEditTextNotes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_edit_card);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mLayout = (LinearLayout) findViewById(R.id.create_edit_card_layout);
        mButtonChooseImage = (Button) findViewById(R.id.button_choose_image);
        mImageViewCardImage = (ImageView) findViewById(R.id.imageView_card_image);
        mTextInputLayoutCardName = (TextInputLayout) findViewById(R.id.textInputLayout_card_name);
        mEditTextCardName = mTextInputLayoutCardName.getEditText();
        mTextInputLayoutBarcode = (TextInputLayout) findViewById(R.id.textInputLayout_barcode);
        mEditTextBarcode = mTextInputLayoutBarcode.getEditText();
        mTextInputLayoutBarcodeFormat = (TextInputLayout) findViewById(R.id.textInputLayout_barcode_format);
        mEditTextBarcodeFormat = mTextInputLayoutBarcodeFormat.getEditText();
        mButtonScanBarcode = (Button) findViewById(R.id.button_scan_barcode);
        mTextInputLayoutNotes = (TextInputLayout) findViewById(R.id.textInputLayout_notes);
        mEditTextNotes = mTextInputLayoutNotes.getEditText();

        mEditTextBarcodeFormat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(CreateEditCardActivity.this);
                final String[] barcodeFormats = getBarcodeFormats();
                builder.setTitle(R.string.pick_barcode_format)
                        .setItems(barcodeFormats, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                mEditTextBarcodeFormat.setText(barcodeFormats[which]);
                            }
                        });
                builder.show();
            }
        });

        mCard = getIntent().getParcelableExtra(CARD);
        mIsEditing = mCard != null;

        if (mIsEditing) {
            if (mCard.getImageUri() != null) {
                mButtonChooseImage.setText(R.string.button_remove_image);
                mImageUri = mCard.getImageUri();
                mImageViewCardImage.setImageURI(mImageUri);
                mImageViewCardImage.setVisibility(View.VISIBLE);
            }
            if (!isStringEmpty(mCard.getName())) {
                mEditTextCardName.setText(mCard.getName());
            }
            if (!isStringEmpty(mCard.getBarcode())) {
                mEditTextBarcode.setText(mCard.getBarcode());
            }
            if (mCard.getBarcodeFormat() != null) {
                mEditTextBarcodeFormat.setText(mCard.getBarcodeFormat().toString());
            }
            if (!isStringEmpty(mCard.getNotes())) {
                mEditTextNotes.setText(mCard.getNotes());
            }
        }

        mEditTextCardName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mTextInputLayoutCardName.setError(null);
            }
            @Override
            public void afterTextChanged(Editable s) {}
        });

        mEditTextBarcode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mTextInputLayoutBarcode.setError(null);
            }
            @Override
            public void afterTextChanged(Editable s) {}
        });

        mButtonChooseImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mImageUri == null) {
                    startCameraIntent();
                } else {
                    mImageViewCardImage.setImageURI(null);
                    mImageViewCardImage.setVisibility(View.GONE);
                    mImageUri = null;
                    mButtonChooseImage.setText(R.string.button_capture_image);
                }
            }
        });

        mButtonScanBarcode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IntentIntegrator integrator = new IntentIntegrator(CreateEditCardActivity.this);
                integrator.setBarcodeImageEnabled(true);
                integrator.initiateScan();
            }
        });

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name = mEditTextCardName.getText().toString();
                String barcode = mEditTextBarcode.getText().toString();
                String barcodeFormat = mEditTextBarcodeFormat.getText().toString();
                String notes = mEditTextNotes.getText().toString();
                if (validate(name, barcode, barcodeFormat)) {
                    SQLiteDatabase db = DbHelper.getInstance(CreateEditCardActivity.this).getWritableDatabase();
                    ContentValues values = new ContentValues();
                    values.put(CardContract.CardEntry.COLUMN_NAME_NAME, name);
                    values.put(CardContract.CardEntry.COLUMN_NAME_BARCODE, barcode);
                    values.put(CardContract.CardEntry.COLUMN_NAME_BARCODE_FORMAT, barcodeFormat);
                    values.put(CardContract.CardEntry.COLUMN_NAME_NOTES, notes);
                    if (mImageUri != null) values.put(CardContract.CardEntry.COLUMN_NAME_IMAGE_URI, mImageUri.getPath());

                    long returnVal;
                    if (!mIsEditing) {
                        returnVal = db.insert(CardContract.CardEntry.TABLE_NAME, null, values);
                    } else {
                        String selection = CardContract.CardEntry._ID + " LIKE ?";
                        String[] selectionArgs = { String.valueOf(mCard.getId()) };
                        returnVal = db.update(CardContract.CardEntry.TABLE_NAME, values, selection, selectionArgs);
                    }

                    if (returnVal > 0) {
                        setResult(RESULT_OK);
                        finish();
                    } else {
                        Snackbar.make(mLayout, "Error!", Snackbar.LENGTH_SHORT).show();
                    }
                }
            }
        });

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            if (mIsEditing) {
                actionBar.setTitle(R.string.edit_card);
            }
        }
    }

    private void startCameraIntent() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_DENIED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, REQUEST_CODE_PERMISSION_CAMERA);
        } else {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            mImageUri = FileUtil.getOutputImageFileUri(CreateEditCardActivity.this);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageUri);
            startActivityForResult(intent, REQUEST_CODE_CAPUTRE_IMAGE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == REQUEST_CODE_PERMISSION_CAMERA) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                startCameraIntent();
            }
        }

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_CAPUTRE_IMAGE) {
            if (resultCode == RESULT_OK) {
                mImageViewCardImage.setVisibility(View.VISIBLE);
                mImageViewCardImage.setImageURI(mImageUri);
                mButtonChooseImage.setText(R.string.button_remove_image);
            } else if (resultCode == RESULT_CANCELED) {
                // user cancelled the image capture
            } else {
                SnackUtil.showSnackBar(mLayout, R.string.error_image_capture);
            }
        }

        if (requestCode == IntentIntegrator.REQUEST_CODE) {
            IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
            if (scanResult != null) {
                mEditTextBarcode.setText(scanResult.getContents());
                mEditTextBarcodeFormat.setText(scanResult.getFormatName());
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    private boolean validate(String name, String barcode, String barcodeFormat) {
        boolean success = true;
        if (isStringEmpty(name)) {
            success = false;
            String text = String.format(getString(R.string.error_empty_string), getString(R.string.card_name));
            mTextInputLayoutCardName.setError(text);
        }
        if (isStringEmpty(barcode)) {
            success = false;
            String text = String.format(getString(R.string.error_empty_string), getString(R.string.barcode));
            mTextInputLayoutBarcode.setError(text);
        }
        if (isStringEmpty(barcodeFormat)) {
            success = false;
            String text = String.format(getString(R.string.error_empty_string), getString(R.string.barcode_format));
            mTextInputLayoutBarcodeFormat.setError(text);
        }

        return success;
    }

    private boolean isStringEmpty(String string) {
        return string == null || string.isEmpty();
    }

    private String[] getBarcodeFormats() {
        BarcodeFormat[] barcodeFormats = BarcodeFormat.values();
        String[] strings = new String[barcodeFormats.length];
        for (int i = 0; i < strings.length; i++) {
            strings[i] = barcodeFormats[i].toString();
        }
        return strings;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
