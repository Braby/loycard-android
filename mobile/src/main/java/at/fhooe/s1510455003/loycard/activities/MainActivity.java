package at.fhooe.s1510455003.loycard.activities;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.PutDataMapRequest;
import com.google.android.gms.wearable.PutDataRequest;
import com.google.android.gms.wearable.Wearable;
import com.google.zxing.BarcodeFormat;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import at.fhooe.s1510455003.loycard.R;
import at.fhooe.s1510455003.loycard.cards.CardsAdapter;
import at.fhooe.s1510455003.loycard.data.Card;
import at.fhooe.s1510455003.loycard.db.CardContract;
import at.fhooe.s1510455003.loycard.db.DbHelper;
import at.fhooe.s1510455003.loycard.util.FileUtil;
import at.fhooe.s1510455003.loycard.util.ResultCodes;
import at.fhooe.s1510455003.loycard.util.SnackUtil;

public class MainActivity extends AppCompatActivity {
    public static final int REQUEST_CODE_CARD_DETAIL = 1;

    private RecyclerView mRecyclerViewCards;
    private GridLayoutManager mGridLayoutMgr;
    private RecyclerView.Adapter mAdapter;
    private TextView mTextViewNoCards;

    private List<Card> mCards;
    private GoogleApiClient mGoogleApiClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mCards = new ArrayList<>();

        mGridLayoutMgr = new GridLayoutManager(this, 3);
        mAdapter = new CardsAdapter(this, mCards);

        mRecyclerViewCards = (RecyclerView) findViewById(R.id.recycler_view_cards);
        mRecyclerViewCards.setLayoutManager(mGridLayoutMgr);
        mRecyclerViewCards.setAdapter(mAdapter);

        mTextViewNoCards = (TextView) findViewById(R.id.text_view_no_cards);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, CreateEditCardActivity.class);
                startActivityForResult(intent, REQUEST_CODE_CARD_DETAIL);
            }
        });

        mGoogleApiClient = new GoogleApiClient.Builder(MainActivity.this).addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
            @Override
            public void onConnected(Bundle bundle) {
                Log.d("wear", "onConnected: " + bundle);
            }

            @Override
            public void onConnectionSuspended(int i) {
                Log.d("wear", "onConnectionSuspended: " + i);
            }
        }).addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
            @Override
            public void onConnectionFailed(ConnectionResult connectionResult) {
                Log.d("wear", "onConnectionFailed: " + connectionResult);
            }
        }).addApi(Wearable.API).build();
    }

    @Override
    protected void onResume() {
        fetchCardsFromDb();
        super.onResume();
    }

    private void fetchCardsFromDb() {
        mCards.clear();
        SQLiteDatabase db = DbHelper.getInstance(this).getReadableDatabase();
        String[] projection = {
                CardContract.CardEntry._ID,
                CardContract.CardEntry.COLUMN_NAME_NAME,
                CardContract.CardEntry.COLUMN_NAME_BARCODE,
                CardContract.CardEntry.COLUMN_NAME_BARCODE_FORMAT,
                CardContract.CardEntry.COLUMN_NAME_IMAGE_URI
        };

        Cursor cursor = db.query(CardContract.CardEntry.TABLE_NAME, projection, null, null, null, null, null);
        if (cursor.moveToFirst()) {
            do {
                Card card = new Card();
                card.setId(cursor.getLong(cursor.getColumnIndex(CardContract.CardEntry._ID)));
                card.setName(cursor.getString(cursor.getColumnIndex(CardContract.CardEntry.COLUMN_NAME_NAME)));
                card.setBarcode(cursor.getString(cursor.getColumnIndex(CardContract.CardEntry.COLUMN_NAME_BARCODE)));
                String barcodeFormatString = cursor.getString(cursor.getColumnIndex(CardContract.CardEntry.COLUMN_NAME_BARCODE_FORMAT));
                if (barcodeFormatString != null) card.setBarcodeFormat(BarcodeFormat.valueOf(barcodeFormatString));
                String uriString = cursor.getString(cursor.getColumnIndex(CardContract.CardEntry.COLUMN_NAME_IMAGE_URI));
                if (uriString != null) card.setImageUri(Uri.parse(uriString));
                mCards.add(card);
            } while (cursor.moveToNext());
        }
        cursor.close();

        if (mCards.size() > 0) {
            mTextViewNoCards.setVisibility(View.GONE);
        } else {
            mTextViewNoCards.setVisibility(View.VISIBLE);
        }

        mAdapter.notifyDataSetChanged();

        syncCardsWithWearable();

        deleteUnusedImages();
    }

    private void syncCardsWithWearable() {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                ConnectionResult connectionResult = mGoogleApiClient.blockingConnect(5, TimeUnit.SECONDS);
                if (!connectionResult.isSuccess()){
                    Log.e("wear", "Failed to connect to GoogleApiClient.");
                    return null;
                }

                List<Node> connectedNodes = Wearable.NodeApi.getConnectedNodes(mGoogleApiClient).await().getNodes();
                if (connectedNodes.size() > 0) {
                    for (Card card : mCards) {
                        PutDataMapRequest putDataMapRequest = PutDataMapRequest.create("/card/" + card.getId());
                        putDataMapRequest.getDataMap().putAll(card.getDataMapForWearable());
                        PutDataRequest putDataRequest = putDataMapRequest.asPutDataRequest().setUrgent();
                        Wearable.DataApi.putDataItem(mGoogleApiClient, putDataRequest);
                        Log.d("wear", "sent card " + card.getName());
                    }
                }
                return null;
            }
        }.execute();
    }

    private void syncDeletedCard(long cardId) {
        ConnectionResult connectionResult = mGoogleApiClient.blockingConnect(5, TimeUnit.SECONDS);
        if (!connectionResult.isSuccess()){
            Log.e("wear", "Failed to connect to GoogleApiClient.");
            return;
        }
        PutDataRequest putDataRequest = PutDataRequest.create("/card/" + cardId).setUrgent();
        Wearable.DataApi.deleteDataItems(mGoogleApiClient, putDataRequest.getUri());
        Log.d("wear", "delete card " + cardId);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_CARD_DETAIL) {
            switch (resultCode) {
                case RESULT_OK:
                    fetchCardsFromDb();
                    break;
                case ResultCodes.RESULT_CODE_ERROR:
                    SnackUtil.showSnackBar(mRecyclerViewCards, R.string.error_general);
                    break;
                case ResultCodes.RESULT_CODE_DELETED:
                    final long cardId = data.getLongExtra(CardDetailsActivity.CARD_ID, -1);
                    if (cardId != -1) new AsyncTask<Void, Void, Void>() {
                        @Override
                        protected Void doInBackground(Void... params) {
                            syncDeletedCard(cardId);
                            return null;
                        }
                    }.execute();
                    fetchCardsFromDb();
                    break;
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    private void deleteUnusedImages() {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                File mediaStorageDir = FileUtil.getMediaStorageDir(MainActivity.this);
                File[] files = mediaStorageDir.listFiles();
                if (files != null) {
                    for (File f : files) {
                        boolean isUnused = true;
                        for (Card card : mCards) {
                            if (card.getImageUri() != null && card.getImageUri().equals(Uri.parse(f.getPath()))) {
                                isUnused = false;
                            }
                        }
                        if (isUnused) {
                            boolean isDeleted = f.delete();
                            if (isDeleted) {
                                Log.d("cardImage", "image deleted");
                            }
                        }
                    }
                }
                return null;
            }
        }.execute();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
