package at.fhooe.s1510455003.loycard.cards;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import at.fhooe.s1510455003.loycard.R;
import at.fhooe.s1510455003.loycard.activities.CardDetailsActivity;
import at.fhooe.s1510455003.loycard.activities.MainActivity;
import at.fhooe.s1510455003.loycard.data.Card;

/**
 * Created by Stefan on 07.01.2016.
 */
public class CardsAdapter extends RecyclerView.Adapter<CardsAdapter.CardViewHolder> {
    private List<Card> mCards;
    private Activity mActivity;

    public CardsAdapter(Activity activity, List<Card> cards) {
        mActivity = activity;
        mCards = cards;
    }

    @Override
    public CardViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_element_card, parent, false);
        CardViewHolder vh = new CardViewHolder((LinearLayout)v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final CardViewHolder holder, int position) {
        if (position >= mCards.size() || position < 0) return;

        final Card card = mCards.get(position);
        if (card != null) {
            holder.mCardId = card.getId();
            holder.mActivity = mActivity;
            holder.mTextView.setText(card.getName());
            if (card.getImageUri() != null) {
                new AsyncTask<Uri, Void, Bitmap>() {
                    @Override
                    protected Bitmap doInBackground(Uri... params) {
                        if (params.length > 0 && params[0] != null) {
                            return BitmapFactory.decodeFile(params[0].getPath());
                        }
                        return null;
                    }
                    @Override
                    protected void onPostExecute(Bitmap bitmap) {
                        holder.mImageView.setImageBitmap(bitmap);
                    }
                }.execute(card.getImageUri());
            }
        }
    }

    @Override
    public int getItemCount() {
        return mCards.size();
    }

    public static class CardViewHolder extends RecyclerView.ViewHolder {
        public long mCardId;
        public Activity mActivity;
        public LinearLayout mCardView;
        public ImageView mImageView;
        public TextView mTextView;
        public CardViewHolder(LinearLayout cardView) {
            super(cardView);
            mCardView = cardView;
            mImageView = (ImageView) mCardView.findViewById(R.id.list_element_card_imageView);
            mTextView = (TextView) mCardView.findViewById(R.id.list_element_card_textView);

            mCardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mActivity, CardDetailsActivity.class);
                    intent.putExtra(CardDetailsActivity.CARD_ID, mCardId);
                    mActivity.startActivityForResult(intent, MainActivity.REQUEST_CODE_CARD_DETAIL);
                }
            });
        }
    }
}
