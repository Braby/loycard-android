package at.fhooe.s1510455003.loycard.data;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.android.gms.wearable.Asset;
import com.google.android.gms.wearable.DataMap;
import com.google.zxing.BarcodeFormat;

import java.io.ByteArrayOutputStream;

/**
 * Created by Stefan on 07.01.2016.
 */
public class Card implements Parcelable {
    private static final String PACKAGE = "at.fhooe.s1510455003.loycard";
    private static final String KEY_ID = PACKAGE + ".id";
    private static final String KEY_NAME = PACKAGE + ".name";
    private static final String KEY_IMAGE = PACKAGE + ".image";
    private static final String KEY_BARCODE = PACKAGE + ".barcode";
    private static final String KEY_BARCODE_FORMAT = PACKAGE + ".barcodeFormat";

    private long mId;
    private String mName;
    private Uri mImageUri;
    private String mBarcode;
    private BarcodeFormat mBarcodeFormat;
    private String mNotes;

    public Card() {}

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(mId);
        dest.writeString(mName);
        dest.writeParcelable(mImageUri, flags);
        dest.writeString(mBarcode);
        dest.writeSerializable(mBarcodeFormat);
        dest.writeString(mNotes);
    }

    public Card(Parcel source) {
        mId = source.readLong();
        mName = source.readString();
        mImageUri = source.readParcelable(Uri.class.getClassLoader());
        mBarcode = source.readString();
        mBarcodeFormat = (BarcodeFormat) source.readSerializable();
        mNotes = source.readString();
    }

    public static final Parcelable.Creator<Card> CREATOR = new Creator<Card>() {
        @Override
        public Card createFromParcel(Parcel source) {
            return new Card(source);
        }

        @Override
        public Card[] newArray(int size) {
            return new Card[size];
        }
    };

    public DataMap getDataMapForWearable() {
        DataMap dataMap = new DataMap();
        dataMap.putLong(KEY_ID, mId);
        dataMap.putString(KEY_NAME, mName);
        dataMap.putString(KEY_BARCODE, mBarcode);
        dataMap.putString(KEY_BARCODE_FORMAT, mBarcodeFormat.toString());
        return dataMap;
    }

    public long getId() {
        return mId;
    }

    public void setId(long id) {
        mId = id;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public Uri getImageUri() {
        return mImageUri;
    }

    public void setImageUri(Uri imageUri) {
        mImageUri = imageUri;
    }

    public String getBarcode() {
        return mBarcode;
    }

    public void setBarcode(String barcode) {
        mBarcode = barcode;
    }

    public BarcodeFormat getBarcodeFormat() {
        return mBarcodeFormat;
    }

    public void setBarcodeFormat(BarcodeFormat barcodeFormat) {
        mBarcodeFormat = barcodeFormat;
    }

    public String getNotes() {
        return mNotes;
    }

    public void setNotes(String notes) {
        mNotes = notes;
    }
}
