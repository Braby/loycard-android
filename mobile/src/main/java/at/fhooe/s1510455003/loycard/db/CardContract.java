package at.fhooe.s1510455003.loycard.db;

import android.provider.BaseColumns;

/**
 * Created by Stefan on 07.01.2016.
 */
public final class CardContract {
    public CardContract() {}

    public static abstract class CardEntry implements BaseColumns {
        public static final String TABLE_NAME = "card";
        public static final String COLUMN_NAME_NAME = "name";
        public static final String COLUMN_NAME_BARCODE = "barcode";
        public static final String COLUMN_NAME_BARCODE_FORMAT = "barcodeFormat";
        public static final String COLUMN_NAME_NOTES = "notes";
        public static final String COLUMN_NAME_IMAGE_URI = "imageUri";
    }

    private static final String TEXT_TYPE = " TEXT";
    private static final String BLOB_TYPE = " BLOB";
    private static final String COMMA_SEP = ",";
    public static final String SQL_CREATE_CARDS =
            "CREATE TABLE " + CardEntry.TABLE_NAME + " (" +
                    CardEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT" + COMMA_SEP +
                    CardEntry.COLUMN_NAME_NAME + TEXT_TYPE + COMMA_SEP +
                    CardEntry.COLUMN_NAME_BARCODE + TEXT_TYPE + COMMA_SEP +
                    CardEntry.COLUMN_NAME_BARCODE_FORMAT + TEXT_TYPE + COMMA_SEP +
                    CardEntry.COLUMN_NAME_NOTES + TEXT_TYPE + COMMA_SEP +
                    CardEntry.COLUMN_NAME_IMAGE_URI + TEXT_TYPE + " )";

    public static final String SQL_DELETE_CARDS = "DROP TABLE IF EXISTS " + CardEntry.TABLE_NAME;
}
