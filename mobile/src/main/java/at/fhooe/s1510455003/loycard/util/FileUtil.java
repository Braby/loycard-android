package at.fhooe.s1510455003.loycard.util;

import android.content.Context;
import android.net.Uri;
import android.os.Environment;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import at.fhooe.s1510455003.loycard.R;

/**
 * Created by Stefan on 09.01.2016.
 */
public class FileUtil {
    public static File getMediaStorageDir(Context context) {
        return new File(context.getExternalFilesDir(Environment.DIRECTORY_PICTURES), context.getString(R.string.app_name));
    }

    public static Uri getOutputImageFileUri(Context context) {
        File mediaStorageDir = getMediaStorageDir(context);
        if (!mediaStorageDir.exists() && !mediaStorageDir.mkdirs()) return null;

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File mediaFile = new File(mediaStorageDir.getPath() + File.separator + timeStamp + ".jpg");
        return Uri.fromFile(mediaFile);
    }
}
