package at.fhooe.s1510455003.loycard.util;

import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.TextView;

import at.fhooe.s1510455003.loycard.R;

/**
 * Created by Stefan on 04.02.2016.
 */
public class SnackUtil {
    public static void showSnackBar(View view, int stringRes) {
        Snackbar snackbar = Snackbar.make(view, stringRes, Snackbar.LENGTH_LONG);
        TextView textView = (TextView) snackbar.getView().findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(view.getResources().getColor(R.color.colorAccent));
        snackbar.show();
    }
}
