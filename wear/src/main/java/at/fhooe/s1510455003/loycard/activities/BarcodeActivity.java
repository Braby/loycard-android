package at.fhooe.s1510455003.loycard.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.wearable.activity.WearableActivity;
import android.support.wearable.view.BoxInsetLayout;
import android.support.wearable.view.CircularButton;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.WriterException;

import at.fhooe.s1510455003.loycard.R;
import at.fhooe.s1510455003.loycard.data.Card;
import at.fhooe.s1510455003.loycard.util.BarcodeUtil;
import at.fhooe.s1510455003.loycard.util.ResultCodes;

public class BarcodeActivity extends WearableActivity {
    public static final String EXTRA_CARD = "card";
    private static final int REQUEST_CODE_MAPS = 10;

    private LinearLayout mLinearLayout;
    private TextView mTextViewCardName;
    private ImageView mImageViewBarcode;
    private CircularButton mButtonMap;

    private Card mCard;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_barcode);
        setAmbientEnabled();

        mLinearLayout = (LinearLayout) findViewById(R.id.barcode_activity_linear_layout);
        mTextViewCardName = (TextView) findViewById(R.id.barcode_activity_card_name);
        mImageViewBarcode = (ImageView) findViewById(R.id.barcode_activity_barcode);
        mButtonMap = (CircularButton) findViewById(R.id.barcode_activity_map_button);

        mCard = getIntent().getParcelableExtra(EXTRA_CARD);

        if (mCard != null) {
            mTextViewCardName.setText(mCard.getName());
            mButtonMap.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(BarcodeActivity.this, MapsActivity.class);
                    i.putExtra(MapsActivity.EXTRA_CARD_NAME, mCard.getName());
                    startActivityForResult(i, REQUEST_CODE_MAPS);
                }
            });
        }
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (hasFocus) {
            try {
                Bitmap bitmap = BarcodeUtil.encodeAsBitmap(mCard.getBarcode(), mCard.getBarcodeFormat(), mLinearLayout.getWidth(), mImageViewBarcode.getHeight());
                mImageViewBarcode.setImageBitmap(bitmap);
            } catch (WriterException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_MAPS && resultCode == ResultCodes.RESULT_CODE_ERROR) {
            Toast.makeText(BarcodeActivity.this, R.string.error_general, Toast.LENGTH_LONG).show();
        }

        super.onActivityResult(requestCode, resultCode, data);
    }
}
