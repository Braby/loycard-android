package at.fhooe.s1510455003.loycard.activities;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.wearable.activity.WearableActivity;
import android.support.wearable.view.WatchViewStub;
import android.support.wearable.view.WearableListView;
import android.view.View;
import android.widget.TextView;

import com.google.zxing.BarcodeFormat;

import java.util.ArrayList;
import java.util.List;

import at.fhooe.s1510455003.loycard.R;
import at.fhooe.s1510455003.loycard.cards.CardsAdapter;
import at.fhooe.s1510455003.loycard.data.Card;
import at.fhooe.s1510455003.loycard.db.CardContract;
import at.fhooe.s1510455003.loycard.db.DbHelper;

public class MainActivity extends WearableActivity {
    private WearableListView mListView;
    private TextView mTextViewNoCards;
    private List<Card> mCards;
    private WearableListView.Adapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setAmbientEnabled();

        final WatchViewStub stub = (WatchViewStub) findViewById(R.id.watch_view_stub);

        mCards = new ArrayList<>();
        mAdapter = new CardsAdapter(mCards, MainActivity.this);

        stub.setOnLayoutInflatedListener(new WatchViewStub.OnLayoutInflatedListener() {
            @Override
            public void onLayoutInflated(WatchViewStub stub) {
                mListView = (WearableListView) stub.findViewById(R.id.cards_list_view);
                mListView.setAdapter(mAdapter);

                mTextViewNoCards = (TextView) stub.findViewById(R.id.text_view_no_cards);
                setNoCardsVisibility();
            }
        });
    }

    @Override
    protected void onResume() {
        fetchCardsFromDb();
        super.onResume();
    }

    private void fetchCardsFromDb() {
        mCards.clear();
        SQLiteDatabase db = DbHelper.getInstance(this).getReadableDatabase();
        Cursor cursor = db.query(CardContract.CardEntry.TABLE_NAME, null, null, null, null, null, null);
        if (cursor.moveToFirst()) {
            do {
                Card card = new Card();
                card.setId(cursor.getLong(cursor.getColumnIndex(CardContract.CardEntry._ID)));
                card.setName(cursor.getString(cursor.getColumnIndex(CardContract.CardEntry.COLUMN_NAME_NAME)));
                card.setBarcode(cursor.getString(cursor.getColumnIndex(CardContract.CardEntry.COLUMN_NAME_BARCODE)));
                String barcodeFormatString = cursor.getString(cursor.getColumnIndex(CardContract.CardEntry.COLUMN_NAME_BARCODE_FORMAT));
                if (barcodeFormatString != null)
                    card.setBarcodeFormat(BarcodeFormat.valueOf(barcodeFormatString));
                mCards.add(card);
            } while (cursor.moveToNext());
        }
        cursor.close();

        setNoCardsVisibility();
        mAdapter.notifyDataSetChanged();
    }

    private void setNoCardsVisibility() {
        if (mTextViewNoCards == null) return;
        if (mCards != null && mCards.size() > 0) {
            mTextViewNoCards.setVisibility(View.GONE);
        } else {
            mTextViewNoCards.setVisibility(View.VISIBLE);
        }
    }
}
