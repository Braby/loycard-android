package at.fhooe.s1510455003.loycard.activities;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.wearable.activity.WearableActivity;
import android.support.wearable.view.DismissOverlayView;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.location.places.AutocompletePredictionBuffer;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import at.fhooe.s1510455003.loycard.R;
import at.fhooe.s1510455003.loycard.util.ResultCodes;

public class MapsActivity extends WearableActivity implements OnMapReadyCallback {
    public static final String EXTRA_CARD_NAME = "cardName";
    private static final int REQUEST_CODE_LOCATION = 50;

    private DismissOverlayView mDismissOverlayView;
    private MapFragment mMapFragment;

    private GoogleMap mMap;
    private GoogleApiClient mGoogleApiClient;
    private boolean mLocationEnabled = false;

    private String mCardName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        setAmbientEnabled();

        mCardName = getIntent().getStringExtra(EXTRA_CARD_NAME);
        if (mCardName == null) finishWithError();

        mDismissOverlayView = (DismissOverlayView) findViewById(R.id.dismiss_overlay);
        mDismissOverlayView.setIntroText(R.string.intro_text);
        mDismissOverlayView.showIntroIfNecessary();

        mMapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
        mMapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
            @Override
            public void onMapLongClick(LatLng latLng) {
                mDismissOverlayView.show();
            }
        });
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(47.61, 13.782778), 5));
        initMapWithLocation();
    }

    private void initMapWithLocation() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mLocationEnabled = true;
            initMap();
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_CODE_LOCATION);
        }
    }

    private void initMap() {
        GoogleApiClient.ConnectionCallbacks connectionCallbacks = new GoogleApiClient.ConnectionCallbacks() {
            @Override
            public void onConnected(Bundle bundle) {
                LatLng coordinates = null;
                if (mLocationEnabled) {
                    try {
                        mMap.setMyLocationEnabled(true);
                        Location lastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
                        if (lastLocation != null) {
                            coordinates = new LatLng(lastLocation.getLatitude(), lastLocation.getLongitude());
                            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(coordinates, 10));
                        }
                    } catch (SecurityException ex) {
                        ex.printStackTrace();
                        finishWithError();
                    }
                }

                LatLngBounds bounds;
                if (coordinates == null) {
                    bounds = new LatLngBounds(new LatLng(46, 9), new LatLng(49, 17.5));
                } else {
                    bounds = new LatLngBounds(new LatLng(coordinates.latitude - 1, coordinates.longitude - 1), new LatLng(coordinates.latitude + 1, coordinates.longitude + 1));
                }

                PendingResult<AutocompletePredictionBuffer> result = Places.GeoDataApi.getAutocompletePredictions(
                        mGoogleApiClient, mCardName, bounds, new AutocompleteFilter.Builder().setTypeFilter(AutocompleteFilter.TYPE_FILTER_ESTABLISHMENT).build());
                result.setResultCallback(new ResultCallback<AutocompletePredictionBuffer>() {
                    @Override
                    public void onResult(AutocompletePredictionBuffer autocompletePredictions) {
                        if (autocompletePredictions != null) {
                            Iterator<AutocompletePrediction> predictionIterator = autocompletePredictions.iterator();
                            List<String> placeIds = new ArrayList<>();
                            while (predictionIterator.hasNext()) {
                                placeIds.add(predictionIterator.next().getPlaceId());
                            }
                            autocompletePredictions.release();

                            if (placeIds.size() > 0) {
                                Places.GeoDataApi.getPlaceById(mGoogleApiClient, placeIds.toArray(new String[placeIds.size()]))
                                        .setResultCallback(new ResultCallback<PlaceBuffer>() {
                                            @Override
                                            public void onResult(PlaceBuffer places) {
                                                Iterator<Place> placeIterator = places.iterator();
                                                if (!placeIterator.hasNext()) {
                                                    showNoStoresToast();
                                                }
                                                while (placeIterator.hasNext()) {
                                                    Place place = placeIterator.next();
                                                    LatLng latLng = place.getLatLng();
                                                    mMap.addMarker(new MarkerOptions().position(latLng).title(place.getName().toString()));
                                                }
                                                places.release();
                                            }
                                        });
                            } else {
                                showNoStoresToast();
                            }
                        }
                    }
                });
            }

            @Override
            public void onConnectionSuspended(int i) {

            }
        };

        GoogleApiClient.OnConnectionFailedListener connectionFailedListener = new GoogleApiClient.OnConnectionFailedListener() {
            @Override
            public void onConnectionFailed(ConnectionResult connectionResult) {
                Log.e("GoogleApiClient", "connection failed: " + connectionResult.getErrorMessage());
                finishWithError();
            }
        };

        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(connectionCallbacks)
                    .addOnConnectionFailedListener(connectionFailedListener)
                    .addApi(LocationServices.API)
                    .addApi(Places.GEO_DATA_API)
                    .build();
        }
        mGoogleApiClient.connect();
    }

    private void showNoStoresToast() {
        Toast.makeText(MapsActivity.this, R.string.no_stores, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == REQUEST_CODE_LOCATION) {
            if (permissions.length == 1 && permissions[0] == Manifest.permission.ACCESS_FINE_LOCATION && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                mLocationEnabled = true;
            } else {
                mLocationEnabled = false;
            }
            initMap();
        }
    }

    private void finishWithError() {
        setResult(ResultCodes.RESULT_CODE_ERROR);
        finish();
    }

    @Override
    protected void onStop() {
        if (mGoogleApiClient != null) mGoogleApiClient.disconnect();
        super.onStop();
    }

    @Override
    public void onEnterAmbient(Bundle ambientDetails) {
        super.onEnterAmbient(ambientDetails);
        mMapFragment.onEnterAmbient(ambientDetails);
    }

    @Override
    public void onExitAmbient() {
        super.onExitAmbient();
        mMapFragment.onExitAmbient();
    }
}
