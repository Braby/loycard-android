package at.fhooe.s1510455003.loycard.cards;

import android.app.Activity;
import android.content.Intent;
import android.support.wearable.view.WearableListView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import at.fhooe.s1510455003.loycard.R;
import at.fhooe.s1510455003.loycard.activities.BarcodeActivity;
import at.fhooe.s1510455003.loycard.data.Card;

/**
 * Created by Stefan on 22.01.2016.
 */
public class CardsAdapter extends WearableListView.Adapter {
    private List<Card> mCards;
    private Activity mActivity;

    public CardsAdapter(List<Card> cards, Activity activity) {
        mCards = cards;
        mActivity = activity;
    }

    @Override
    public WearableListView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_element_card, null);
        CardViewHolder vh = new CardViewHolder((LinearLayout)v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final WearableListView.ViewHolder holder, int position) {
        if (position >= mCards.size() || position < 0) return;

        final CardViewHolder cardViewHolder = (CardViewHolder) holder;

        final Card card = mCards.get(position);
        if (card != null) {
            cardViewHolder.mCard = card;
            cardViewHolder.mActivity = mActivity;
            cardViewHolder.mTextView.setText(card.getName());
        }
    }

    @Override
    public int getItemCount() {
        return mCards.size();
    }

    public static class CardViewHolder extends WearableListView.ViewHolder {
        public Card mCard;
        public Activity mActivity;
        public LinearLayout mCardView;
        public ImageView mImageView;
        public TextView mTextView;
        public CardViewHolder(LinearLayout cardView) {
            super(cardView);
            mCardView = cardView;
            mImageView = (ImageView) mCardView.findViewById(R.id.list_element_card_imageView);
            mTextView = (TextView) mCardView.findViewById(R.id.list_element_card_textView);

            mCardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(mActivity, BarcodeActivity.class);
                    i.putExtra(BarcodeActivity.EXTRA_CARD, mCard);
                    mActivity.startActivity(i);
                }
            });
        }
    }
}
