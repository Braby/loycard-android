package at.fhooe.s1510455003.loycard.cards;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.data.FreezableUtils;
import com.google.android.gms.wearable.DataEvent;
import com.google.android.gms.wearable.DataEventBuffer;
import com.google.android.gms.wearable.DataItem;
import com.google.android.gms.wearable.DataMap;
import com.google.android.gms.wearable.DataMapItem;
import com.google.android.gms.wearable.Wearable;
import com.google.android.gms.wearable.WearableListenerService;

import java.util.List;
import java.util.concurrent.TimeUnit;

import at.fhooe.s1510455003.loycard.data.Card;
import at.fhooe.s1510455003.loycard.db.CardContract;
import at.fhooe.s1510455003.loycard.db.DbHelper;

/**
 * Created by Stefan on 04.02.2016.
 */
public class CardsListenerService extends WearableListenerService {
    @Override
    public void onDataChanged(DataEventBuffer dataEventBuffer) {
        Log.d("wear", "onDataChanged");

        GoogleApiClient googleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Wearable.API)
                .build();
        ConnectionResult connectionResult = googleApiClient.blockingConnect(30, TimeUnit.SECONDS);

        if (!connectionResult.isSuccess()) {
            Log.e("wear", "Failed to connect to GoogleApiClient.");
            return;
        }

        final List<DataEvent> events = FreezableUtils.freezeIterable(dataEventBuffer);
        for (DataEvent event : events) {
            DataItem item = event.getDataItem();
            if (item.getUri().getPath().contains("/card/")) {

                if (event.getType() == DataEvent.TYPE_DELETED) {
                    long cardId = Long.parseLong(item.getUri().getPath().split("/card/")[1]);
                    SQLiteDatabase db = DbHelper.getInstance(this).getWritableDatabase();
                    String selection = CardContract.CardEntry._ID + " LIKE ?";
                    String[] selectionArgs = { String.valueOf(cardId) };
                    int count = db.delete(CardContract.CardEntry.TABLE_NAME, selection, selectionArgs);
                    Log.d("wear", "deleted card " + cardId);

                } else if (event.getType() == DataEvent.TYPE_CHANGED) {
                    DataMap dataMap = DataMapItem.fromDataItem(item).getDataMap();
                    Card card = Card.getCard(dataMap);
                    Log.d("wear", "got card " + card.getName());

                    SQLiteDatabase db = DbHelper.getInstance(this).getWritableDatabase();

                    String[] projection = { CardContract.CardEntry._ID };
                    String selection = CardContract.CardEntry._ID + " LIKE ?";
                    String[] selectionArgs = { String.valueOf(card.getId()) };
                    Cursor cursor = db.query(CardContract.CardEntry.TABLE_NAME, projection, selection, selectionArgs, null, null, null);
                    boolean isEditing = cursor.getCount() > 0;
                    cursor.close();

                    ContentValues values = new ContentValues();
                    values.put(CardContract.CardEntry._ID, card.getId());
                    values.put(CardContract.CardEntry.COLUMN_NAME_NAME, card.getName());
                    values.put(CardContract.CardEntry.COLUMN_NAME_BARCODE, card.getBarcode());
                    values.put(CardContract.CardEntry.COLUMN_NAME_BARCODE_FORMAT, card.getBarcodeFormat().toString());

                    long returnVal;
                    if (!isEditing) {
                        returnVal = db.insert(CardContract.CardEntry.TABLE_NAME, null, values);
                    } else {
                        returnVal = db.update(CardContract.CardEntry.TABLE_NAME, values, selection, selectionArgs);
                    }
                }
            }
        }
    }
}
