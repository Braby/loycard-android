package at.fhooe.s1510455003.loycard.cards;

import android.content.Context;
import android.support.wearable.view.WearableListView;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import at.fhooe.s1510455003.loycard.R;

/**
 * Created by Stefan on 01.02.2016.
 */
public class ListElementCardLayout extends LinearLayout
        implements WearableListView.OnCenterProximityListener {

    private ImageView mImageView;
    private TextView mTextView;

    public ListElementCardLayout(Context context) {
        this(context, null);
    }

    public ListElementCardLayout(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ListElementCardLayout(Context context, AttributeSet attrs,
                                 int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        mImageView = (ImageView) findViewById(R.id.list_element_card_imageView);
        mTextView = (TextView) findViewById(R.id.list_element_card_textView);
    }

    @Override
    public void onCenterPosition(boolean animate) {
        mTextView.setTextColor(getResources().getColor(R.color.white));
    }

    @Override
    public void onNonCenterPosition(boolean animate) {
        mTextView.setTextColor(getResources().getColor(R.color.colorAccent));
    }
}
