package at.fhooe.s1510455003.loycard.data;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.android.gms.wearable.DataMap;
import com.google.zxing.BarcodeFormat;

/**
 * Created by Stefan on 07.01.2016.
 */
public class Card implements Parcelable {
    private static final String PACKAGE = "at.fhooe.s1510455003.loycard";
    private static final String KEY_ID = PACKAGE + ".id";
    private static final String KEY_NAME = PACKAGE + ".name";
    private static final String KEY_BARCODE = PACKAGE + ".barcode";
    private static final String KEY_BARCODE_FORMAT = PACKAGE + ".barcodeFormat";

    private long mId;
    private String mName;
    private String mBarcode;
    private BarcodeFormat mBarcodeFormat;

    public Card() {}

    public static Card getCard(DataMap dataMap) {
        Card card = new Card();
        card.setId(dataMap.getLong(KEY_ID, -1));
        card.setName(dataMap.getString(KEY_NAME));
        card.setBarcode(dataMap.getString(KEY_BARCODE));
        card.setBarcodeFormat(BarcodeFormat.valueOf(dataMap.getString(KEY_BARCODE_FORMAT)));
        return card;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(mId);
        dest.writeString(mName);
        dest.writeString(mBarcode);
        dest.writeSerializable(mBarcodeFormat);
    }

    public Card(Parcel source) {
        mId = source.readLong();
        mName = source.readString();
        mBarcode = source.readString();
        mBarcodeFormat = (BarcodeFormat) source.readSerializable();
    }

    public static final Parcelable.Creator<Card> CREATOR = new Parcelable.Creator<Card>() {
        @Override
        public Card createFromParcel(Parcel source) {
            return new Card(source);
        }

        @Override
        public Card[] newArray(int size) {
            return new Card[size];
        }
    };

    public long getId() {
        return mId;
    }

    public void setId(long id) {
        mId = id;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getBarcode() {
        return mBarcode;
    }

    public void setBarcode(String barcode) {
        mBarcode = barcode;
    }

    public BarcodeFormat getBarcodeFormat() {
        return mBarcodeFormat;
    }

    public void setBarcodeFormat(BarcodeFormat barcodeFormat) {
        mBarcodeFormat = barcodeFormat;
    }
}
